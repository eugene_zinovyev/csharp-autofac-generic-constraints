namespace AutofacGenericConstraintsPoc
{
    public interface IUserSpecificEntity : IEntity
    {
        int UserId { get; set; }
    }
}
