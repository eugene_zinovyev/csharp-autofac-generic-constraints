namespace AutofacGenericConstraintsPoc
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
