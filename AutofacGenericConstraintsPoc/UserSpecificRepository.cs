using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace AutofacGenericConstraintsPoc
{
    public class UserSpecificRepository<T> : Repository<T> where T: class, IUserSpecificEntity
    {
        private readonly User user;

        public UserSpecificRepository(IEnumerable<IEntity> allEntities, User user)
            : base(allEntities)
        {
            this.user = user;
        }

        protected override IEnumerable<T> Prefilter()
        {
            return base.Prefilter().Where(x => x.UserId == user.Id);
        }
    }
}
