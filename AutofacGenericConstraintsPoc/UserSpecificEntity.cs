namespace AutofacGenericConstraintsPoc
{
    public class UserSpecificEntity : IUserSpecificEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }
    }
}
