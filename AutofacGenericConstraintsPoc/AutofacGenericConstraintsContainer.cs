﻿using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Core;

namespace AutofacGenericConstraintsPoc
{
    public class AutofacGenericConstraintsContainer
    {
        private readonly User user;
        private readonly IEnumerable<IEntity> entities;
        private readonly IContainer container;

        public AutofacGenericConstraintsContainer()
        {
            user = new User { Id = 12 };
            entities = new IEntity[]
            {
                new Entity { Id = 1 },
                new Entity { Id = 2 },
                new Entity { Id = 3 },
                new UserSpecificEntity { Id = 11, UserId = user.Id },
                new UserSpecificEntity { Id = 12, UserId = user.Id },
                new UserSpecificEntity { Id = 13, UserId = user.Id },
            };

            var builder = new ContainerBuilder();
            var parameter = TypedParameter.From(entities);
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).WithParameter(parameter).InstancePerDependency();
            builder.RegisterGeneric(typeof(UserSpecificRepository<>)).As(typeof(IRepository<>)).WithParameter(parameter).InstancePerDependency();
            builder.RegisterInstance(user).SingleInstance();
            container = builder.Build();
        }

        public IRepository<TEntity> ResolveRepository<TEntity>()
            where TEntity : class, IEntity
        {
            return container.Resolve<IRepository<TEntity>>();
        }
    }
}
