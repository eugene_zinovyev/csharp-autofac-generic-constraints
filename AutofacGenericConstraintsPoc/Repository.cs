using System.Collections.Generic;
using System.Linq;

namespace AutofacGenericConstraintsPoc
{
    public class Repository<T> : IRepository<T> where T: class, IEntity
    {
        private readonly IEnumerable<IEntity> allEntities;

        public Repository(IEnumerable<IEntity> allEntities)
        {
            this.allEntities = allEntities;
        }

        public IReadOnlyCollection<T> Get()
        {
            return Prefilter().ToArray();
        }

        protected virtual IEnumerable<T> Prefilter()
        {
            return allEntities.Where(x => x is T).Cast<T>();
        }
    }
}
