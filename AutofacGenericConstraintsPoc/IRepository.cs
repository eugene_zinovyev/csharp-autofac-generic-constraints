using System.Collections.Generic;

namespace AutofacGenericConstraintsPoc
{
    public interface IRepository<T>
        where T: class, IEntity
    {
        IReadOnlyCollection<T> Get();
    }
}
