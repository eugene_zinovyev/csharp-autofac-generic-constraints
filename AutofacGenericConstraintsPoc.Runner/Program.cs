﻿using System;
using Newtonsoft.Json;

namespace AutofacGenericConstraintsPoc.Runner
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var container = new AutofacGenericConstraintsContainer();

            var regularRepository = container.ResolveRepository<Entity>();
            var regularRepositoryEntities = regularRepository.Get();
            Console.WriteLine(JsonConvert.SerializeObject(regularRepositoryEntities));

            var userSpecificRepository = container.ResolveRepository<UserSpecificEntity>();
            var userSpecificRepositoryEntities = userSpecificRepository.Get();
            Console.WriteLine(JsonConvert.SerializeObject(userSpecificRepositoryEntities));
        }
    }
}
