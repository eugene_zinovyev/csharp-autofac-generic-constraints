﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace AutofacGenericConstraintsPoc.Tests
{
    [TestFixture]
    public class AutofacGenericConstraintsContainerTests
    {
        private AutofacGenericConstraintsContainer container;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            container = new AutofacGenericConstraintsContainer();
        }

        [Test]
        public void RegularRepositoryResolvedCorrectly()
        {
            var respository = container.ResolveRepository<Entity>();
            respository.Should().BeOfType<Repository<Entity>>();
        }

        [Test]
        public void UserSpecificRepositoryResolvedCorrectly()
        {
            var respository = container.ResolveRepository<UserSpecificEntity>();
            respository.Should().BeOfType<UserSpecificRepository<UserSpecificEntity>>();
        }
    }
}
